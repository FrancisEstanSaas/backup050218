//
//  KYCEntity.swift
//  GBA
//
//  Created by Republisys on 05/03/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit

struct KYCInfoEntity: Decodable{
    
    //Personal Details
    var firstName: String = ""
    var lastName: String = ""
    var birthDate: String = ""
    var citezenship: String = ""
    
    //Address
    var currentStreet: String = ""
    var currentState: String = ""
    var currentCity: String = ""
    var currentZipCode: String = ""
    var currentCountry: String = ""
    
    var permanentStreet: String = ""
    var permanentState: String = ""
    var permanentCity: String = ""
    var permanentZipCode: String = ""
    var permanentCountry: String = ""
    
    //Salary
    var sourceOfIncome: String = ""
    
    //Gov't ID
    var govtIDImage: String = ""
    
    //Billing Statement
    var billingStatement: String = ""
    
    init(firstName: String, lastName: String, birthDate: String, citezenship: String, currentStreet: String, currentState: String, currentCity: String, currentZipCode: String, permanentStreet: String, permanentState: String, permanentCity: String, permanentZipCode: String, sourceOfIncome: String, govtIDImage: String, billingStatement: String){
        
        //Personal Info
        self.firstName = firstName
        self.lastName = lastName
        self.birthDate = birthDate
        self.citezenship = citezenship
        
        //Current Add
        self.currentStreet = currentStreet
        self.currentState = currentState
        self.currentCity = currentCity
        self.currentZipCode = currentZipCode
        
        //Present Add
        self.permanentStreet = permanentStreet
        self.permanentState = permanentState
        self.permanentCity = permanentCity
        self.permanentZipCode = permanentZipCode
        
        //Images
        
        self.govtIDImage = govtIDImage
        self.billingStatement = billingStatement
    }
    

}







