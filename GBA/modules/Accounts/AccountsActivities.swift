//
//  AccountsActivities.swift
//  GBA
//
//  Created by Gladys Prado on 9/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

enum AccountsActivities: ViewControllerIdentifier {
    case ListGBAWallets = "ListGBAWallets"
    case ViewAccountDetails = "ViewAccountDetails"
    
    func getPresenter(with viewController: AccountsRootViewController, and wireframe: AccountsWireframe)->RootPresenter?{
        
        switch self {
        case .ListGBAWallets:
            return ListGBAWalletsPresenter(wireframe: wireframe, view: viewController)
        case .ViewAccountDetails:
            return ViewAccountDetailsPresenter(wireframe: wireframe, view: viewController)
        }
        
    }
}
