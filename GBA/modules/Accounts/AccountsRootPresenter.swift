//
//  AccountsRootPresenter.swift
//  GBA
//
//  Created by Gladys Prado on 9/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation

class AccountsRootPresenter: RootPresenter{
    
    var wireframe: AccountsWireframe
    var interactor: (local: AccountsLocalInteractor, remote: RootRemoteInteractor) = (AccountsLocalInteractor(), RootRemoteInteractor())
    var view: AccountsRootViewController
    
    init(wireframe: AccountsWireframe, view: AccountsRootViewController) {
        self.wireframe = wireframe
        self.view = view
    }
}
