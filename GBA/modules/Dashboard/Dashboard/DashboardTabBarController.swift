//
//  DashboardTabbarController.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/21/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class DashboardTabBarController: UITabBarController, UITabBarControllerDelegate{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let nav = self.navigationController else{ fatalError("navigator not found") }
        nav.isNavigationBarHidden = false
        
        guard let dashboardTab = DashboardWireframe(self.navigationController!).getViewController(from: .Dashboardscreen) else { fatalError() }
        dashboardTab.parent?.title = "Dashboard"

        guard let viewController1 = EntryWireframe(self.navigationController!).getViewController(from: .Loginscreen) else { fatalError() }
        viewController1.parent?.title = "Entry"
        
        guard let transfersTab = TransfersWireframe(self.navigationController!).getViewController(from: .TransfersMainView) else { fatalError() }
        transfersTab.parent?.title = "Transfers"
        
        guard let accountsTab = AccountsWireframe(self.navigationController!).getViewController(from: .ListGBAWallets) else { fatalError() }
        accountsTab.parent?.title = "Accounts"
        
        guard let payBillsTab = TransfersWireframe(self.navigationController!).getViewController(from: .PayBillsMainView) else { fatalError() }
        payBillsTab.parent?.title = "Pay Bills"
        
        guard let more = Bundle.main.loadNibNamed("GBA_MoreViewController", owner: self, options: nil)?.first as? GBA_MoreViewController else { fatalError() }
        
        self.viewControllers? = [dashboardTab, transfersTab, accountsTab, payBillsTab, more]
        
        setTabBarItems()
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//        print(self.tabBarController?.selectedIndex)
//        print("tabbar selected")
    }

    func setTabBarItems(){
        
        let dashboardTabBar = (self.tabBar.items?[0])! as UITabBarItem
        dashboardTabBar.image = UIImage(named: "Dashboard_Inactive")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        dashboardTabBar.selectedImage = UIImage(named: "Dashboard_Active")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        dashboardTabBar.title = "Dashboard"
        
        let transfersTabBar = (self.tabBar.items?[1])! as UITabBarItem
        transfersTabBar.image = UIImage(named: "Transfer_Inactive")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        transfersTabBar.selectedImage = UIImage(named: "Transfer_Active")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        transfersTabBar.title = "Transfers"
        
        let accountsTabBar = (self.tabBar.items?[2])! as UITabBarItem
        accountsTabBar.image = UIImage(named: "Accounts_Inactive")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        accountsTabBar.selectedImage = UIImage(named: "Accounts_Active")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        accountsTabBar.title = "Accounts"
        
        let payBillsTabBar = (self.tabBar.items?[3])! as UITabBarItem
        payBillsTabBar.image = UIImage(named: "PayBills_Inactive")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        payBillsTabBar.selectedImage = UIImage(named: "PayBills_Active")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        payBillsTabBar.title = "Pay Bills"
        
        let moreTabBar = (self.tabBar.items?[4])! as UITabBarItem
        moreTabBar.image = UIImage(named: "More_Inactive")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        moreTabBar.selectedImage = UIImage(named: "More_Active")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        moreTabBar.title = "More"
    }
    
    
}
