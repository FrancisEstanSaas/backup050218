//
//  DashboardTransactionHistoryCellView.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/24/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

enum DashboardTransactionHistoryCellViewPosition{
    case top, middle, bottom
}

class DashboardTransactionHistoryCellView: UITableViewCell {

    @IBOutlet fileprivate weak var logo_image: UIImageView!
    @IBOutlet fileprivate weak var time_label: UILabel!
    @IBOutlet fileprivate weak var subjectName_label: UILabel!
    @IBOutlet fileprivate weak var floatingContainer_view: UIView!
    @IBOutlet fileprivate weak var underline_view: UIView!
    
    fileprivate(set) var name: String = ""{
        didSet{ self.subjectName_label.set(value: self.name) }
    }
    
    var position: DashboardTransactionHistoryCellViewPosition?{
        didSet{
            guard let position = position else { return }
            
            self.underline_view.backgroundColor = GBAColor.gray.rawValue

            switch position {
            case .top:
                self.floatingContainer_view.roundedCorners([.topLeft, .topRight], radius: 10)
            case .bottom:
                self.floatingContainer_view.roundedCorners([.bottomLeft, .bottomRight], radius: 10)
                underline_view.backgroundColor = GBAColor.white.rawValue
            case .middle: break
            }
            
            self.layoutSubviews()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

extension DashboardTransactionHistoryCellView{
    @discardableResult
    func set(name: String)->Self{
        self.name = name
        return self
    }
}

fileprivate extension UIView{
    func roundedCorners(_ corners: UIRectCorner, radius: CGFloat){
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
