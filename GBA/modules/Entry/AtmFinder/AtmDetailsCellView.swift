//
//  AtmDetailsCellView.swift
//  GBA
//
//  Created by EDI on 8/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit

enum AtmDetailsCellViewPosition {
    case top, middle, bottom
}

class AtmDetailsCellView: UITableViewCell {

    
    @IBOutlet weak var atmDistance: UILabel!
    @IBOutlet weak var atmName: UILabel!
    @IBOutlet weak var atmAddress: UILabel!
    @IBOutlet weak var floatingContainer_view: UIView!
    
    var position: AtmDetailsCellViewPosition?{
        didSet{
            guard let position = position else { return }
            
//            self.underline_view.backgroundColor = GBAColor.gray.rawValue
            
            switch position {
            case .top:
                self.floatingContainer_view.roundedCorners([.topLeft, .topRight], radius: 10)
            case .bottom:
                self.floatingContainer_view.roundedCorners([.bottomLeft, .bottomRight], radius: 10)
//                underline_view.backgroundColor = GBAColor.white.rawValue
            case .middle: break
            }
            
            self.layoutSubviews()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

fileprivate extension UIView{
    func roundedCorners(_ corners: UIRectCorner, radius: CGFloat){
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
