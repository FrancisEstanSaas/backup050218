//
//  ForgotPasswordPresenter.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/19/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

protocol DataReceivedFromForgotPassword{
    func didReceiveVerificationData(code: String)
}

class ForgotPasswordPresenter: EntryRootPresenter{
    private var mobileNumber: String? = nil
    private var codeForNewPassword: String? = nil
    
    var dataBridge: DataReceivedFromForgotPassword? = nil
    
    private var tryCtr = 0
    
    func sendVerificationCode(to number: String){
        self.mobileNumber = number
        guard let bridge = dataBridge else { fatalError("dataBridge was not implemented in ForgotPasswordPresenter") }
        
        self.interactor.remote.SendVerificationCode(to: number) { (json, statusCode) in
            
            print(json)
            switch statusCode{
            case .fetchSuccess:
                
                guard let textMessage = json["tmp"] as? [String: Any],
                    let _ = textMessage["code"] as? String else{
                        self.wireframe.presentCodeVerificationViewController(from: self.view as! GBAVerificationCodeDelegate,
                                                                             completion: nil,
                                                                             apiCalls: { (_, _) in () }, backAction:
                            { self.view.navigationController?.popToRootViewController(animated: true) })
                        return
                }
                self.tryCtr = 0
                self.wireframe.presentCodeVerificationViewController(from: self.view as! GBAVerificationCodeDelegate, completion: {
                    self.wireframe.navigate(to: .Loginscreen)
                }, apiCalls: { (PIN, view) in
                    self.interactor.remote.SendForgotPasswordVerification(code: PIN, to: number, successHandler: { (json, serverCode) in
                        let newPasswordCode = json["code"] as? String
                        self.tryCtr++
                        
                        switch serverCode{
                        case .fetchSuccess:
                            self.codeForNewPassword = newPasswordCode
                            self.mobileNumber = number
                            
                            self.wireframe.navigate(to: .NewPasswordscreen, with: self)
                            return
                        case .badRequest:
                            if self.tryCtr >= 5{
                                let alert = UIAlertController(title: "Warning", message: "Is \(number) the number you entered?", preferredStyle: .alert)
                                
                                let yes = UIAlertAction(title: "YES", style: .default, handler: { (_) in
                                    alert.dismiss(animated: true, completion: nil)
                                    view.clearPIN()
                                })
                                
                                let no = UIAlertAction(title: "NO", style: .destructive, handler: { (_) in
                                    self.view.navigationController?.popViewController(animated: true)
                                })
                                
                                alert.addAction(yes)
                                alert.addAction(no)
                                
                                self.view.present(alert, animated: true, completion: nil)
                            }else{
                                self.showAlert(with: "Warning",
                                               message: "Incorrect or expired verification code",
                                               completion: { view.clearPIN() })
                                
                            }
                        default:
                            view.clearPIN()
                        }
                    })
                }, backAction: {
                    let alert = UIAlertController(title: "Warning", message: "Are you sure you want to cancel forgot password transaction?", preferredStyle: .alert)
                    let yes = UIAlertAction(title: "Yes", style: .destructive, handler: { (_) in
                        self.view.navigationController?.dismiss(animated: true, completion: nil)
                    })
                    let no = UIAlertAction(title: "No", style: .default, handler: { (_) in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    
                    alert.addAction(no)
                    alert.addAction(yes)
                    
                    self.view.navigationController?.present(alert, animated: true, completion: nil)
                })
            case .badRequest:
                guard let messages = json["message"] as? [String:Any] else{ fatalError("Message not found") }
                
                var message: String?
                
                messages.forEach{
                    message = ($0.value as? [String])?.first
                    return
                }
                
                self.showAlert(with: "Oops", message: message ?? "message not found", completion: { () } )
            default: break
            }
            bridge.didReceiveVerificationData(code: "processed")
        }
    }

    func resendVerificationCode(callback: @escaping ()->()){
        self.interactor.remote.ResendForgotVerificationCode { (reply, statusCode) in
            switch statusCode{
            case .fetchSuccess, .accepted:
                callback()
                self.showAlert(with: "Notice", message: "Please check your mobile for verification", completion: { } )
            default: break
            }
        }
    }
    
    
//    func resendVerificationCode(callback: @escaping ()->()){
//        guard let number = self.mobileNumber else { return }
//        self.interactor.remote.ResendForgotPasswordVerificationCode(mobile: number, successHandler: { (reply, replyCode) in
//            print(reply)
//            print(replyCode.rawValue)
//            print(replyCode)
//
//            callback()
//
//
//            switch replyCode{
//            case .fetchSuccess, .accepted:
//
//                var verificationCode: String?
//
//                guard let message = reply["message"] as? String else{
//                    print(reply)
//                    return
//                }
//
//                if let tmp = reply["tmp"] as? [String: Any],
//                    let code = tmp["code"] as? String{
//                    verificationCode = code
//                }
//
//                self.showAlert(with: verificationCode ?? " ", message: message, completion: { } )
//
//            case .badRequest:
//                guard let message = reply["message"] as? [String: JSON],
//                    let error = message["mobile"]?.first?.value as? String else{
//                        return
//                }
//
//                self.showAlert(with: "Warning", message: error, completion: { } )
//            default: break
//            }
//        })
//    }
    
    func sendNewPassword(newPassword: String, confirm_password: String){
        self.interactor.remote.SendNewPassword(code: self.codeForNewPassword ?? "", password: newPassword, confirm_password: confirm_password) { (json, statusCode) in
            
            switch statusCode{
            case .badRequest:
                guard let message = json["message"] as? [String:[Any]],
                let error = (message.first?.value as? [String])?.first else { return }
                        let alert = UIAlertController(title: "Invalid Input", message: error, preferredStyle: .alert)
                
                let no = UIAlertAction(title: "Ok", style: .cancel) { (_) in
                    alert.dismiss(animated: true, completion: nil)
                }
                
                alert.addAction( no )
                
                self.view.present(alert, animated: true, completion: nil)

                
            case .fetchSuccess:
                guard let message = json["message"] as? String else { return }
                
                self.showAlert(with: "SUCCESS", message: message, completion: { self.wireframe.navigate(to: .Loginscreen) } )
            default: break
            }
        }
    }
}

