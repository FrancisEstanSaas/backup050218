    
//
//  Login.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/14/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import KeychainAccess
import CryptoSwift

    
class LoginViewController: EntryModuleViewController{
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .default }
    
    // mobileNumber_textField && password_textField
    
    @IBOutlet weak var appTitle_label: UILabel!
    @IBOutlet weak var mobileNumber_textField: GBATitledTextField!
    @IBOutlet weak var password_textField: GBATitledTextField!
    @IBOutlet weak var createAccount_label: UILabel!
    @IBOutlet weak var forgotPassword_label: UILabel!
    @IBOutlet weak var login_button: GBAButton!
    
    private let defaultCountry:Countries = .Philippines

    
    
    private var loginForm: LoginFormEntity{
        let uuidString = UIDevice.current.identifierForVendor!.uuidString  //UUID().uuidString
        let uuidToSet = "GBA02-"+"\(uuidString)"
        print(uuidToSet)
        return LoginFormEntity(mobile: mobileNumber, password: password_textField.text, uuid: uuidToSet)
    }
    
    private var mobileNumber: String{
        return mobileNumber_textField.text
    }

    
    var currentPresenter: LoginPresenter{
        guard let prsntr = self.presenter as? LoginPresenter
            else{ fatalError("Error in parsing presenter for RegistrationViewController") }
        return prsntr
    }
    
    fileprivate var user: UserKeyInfo{
        get{
            guard let usr = GBARealm.objects(UserKeyInfo.self).first else{
                fatalError("User not found")
            }
            return usr
        }
    }

    fileprivate var registeredUser: RegisteredUserInfo{
        get{
            guard let usr = GBARealm.objects(RegisteredUserInfo.self).first else{
                fatalError("User not found")
            }
            return usr
        }
    }

    
    override func viewDidLoad() {
        (self._presenter as! LoginPresenter).dataBridgeToView = self
        self.setBackground()
        self.presenter.set(view: self)
        self.createAccount_label.isUserInteractionEnabled = true
        self.forgotPassword_label.isUserInteractionEnabled = true
        
        let createAccount_tap = UITapGestureRecognizer(target: self, action: #selector(createAccount_tapped))
        createAccount_label.gestureRecognizers = [createAccount_tap]
        let forgotPassword_tap = UITapGestureRecognizer(target: self, action: #selector(forgotPassword_tapped))
        forgotPassword_label.gestureRecognizers = [forgotPassword_tap]
        
        self.addPrimaryLogInUser()
        
        self.mobileNumber_textField
            .set(self)
            .set(text: "")
            .set(required: true)
            .set(alignment: .left)
            .set(underlineColor: .gray)
            .set(inputType: .mobileNumber)
            .set(placeholder: "Mobile Number")
        
        self.password_textField
            .set(self)
            .set(text: "")
            .set(required: true)
            .set(security: true)
            .set(alignment: .left)
            .set(returnKeyType: .done)
            .set(inputType: .freeText)
            .set(underlineColor: .gray)
            .set(placeholder: "Password")
        
        self.currentPresenter.countDownTimer != 0 ? self.currentPresenter.checkTryCounter(): ()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = " "
        self.navigationController?.isNavigationBarHidden = true
        self.appTitle_label.textColor = GBAColor.darkGray.rawValue
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.emptyForms()
    }
    
    override func applicationDidBecomeActive() {
        self.emptyForms()
    }
    
    private func emptyForms(){
        self.mobileNumber_textField
            .set(text: "")
            .hideValidation()
        
        self.password_textField
            .set(text: "")
            .hideValidation()
    }
    
    fileprivate func setBackground(){
        let backgroundImage = UIImage(imageLiteralResourceName: "Login_BG").cgImage
        let layer = CALayer()
        let overlay = CAGradientLayer()
        
        layer.frame = UIScreen.main.bounds
        layer.contents = backgroundImage
        layer.contentsGravity = kCAGravityResizeAspectFill
        
        overlay.set(frame: self.view.bounds)
            .set(start: CGPoint(x: 0, y: 0))
            .set(end: CGPoint(x: 0, y: 1))
            .set(colors: [.white, .primaryBlueGreen])
            .set(locations: [0, 1.4])
            .opacity = 0.8
        
        self.view.layer.insertSublayer(layer, at: 0)
        self.view.layer.insertSublayer(overlay, at: 1)
    }
    
    @objc func createAccount_tapped(){
        let nav = UINavigationController()
        EntryWireframe(nav).navigate(to: .Registrationscreen)
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    @objc func forgotPassword_tapped(){
        let nav = UINavigationController()
        EntryWireframe(nav).navigate(to: .ForgotPasswordscreen)
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func support_tapped(_ sender: UIButton) {
        let nav = UINavigationController()
        nav.navigationBar.barTintColor = GBAColor.primaryBlueGreen.rawValue
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        nav.navigationBar.tintColor = GBAColor.white.rawValue
        nav.isNavigationBarHidden = false
        
        EntryWireframe(nav).navigate(to: .Supportscreen)
        self.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func login_tapped(_ sender: GBAButton) {
        if (self.password_textField.isValid() && self.mobileNumber_textField.isValid()) { 
            self.currentPresenter.processLogin(form: loginForm, controller: self)
            
            //New PrimaryUser set.
            self.setNewPrimary(userName: self.mobileNumber_textField.text, userPassword: self.password_textField.text)
        }
    }
    
    @IBAction func navigateATM_tapped(_ sender: UIButton) {
        self.presenter.wireframe.navigate(to: .AtmFinderscreen)
    }
    
    //For Alternative LogInVC
    func logInTypeValidator() {
        let appNav = self.navigationController!
        let logInUserProfile = self.registeredUser //should read registered user object from realm.
        
        if logInUserProfile.logInType == "0" {
            print("Default Log-In Set")
            return
        } else if logInUserProfile.logInType == "1" {
            print("PIN Code Set")
            EntryWireframe(appNav).navigate(to: .PinCodeLogIn)
            
        } else if logInUserProfile.logInType == "2"{
            print("Touch ID Log-In Set")
            EntryWireframe(appNav).navigate(to: .TouchIDLogIn)
            
        } else {
            print("Error in LogIn Type Occured")
            return
        }
    }
    
    func displayCountdown(timer: Int){
        if timer == 0{
            login_button.setTitle("LOGIN", for: .normal)
            login_button.isEnabled = true
            login_button.set(backgroundColor: .primaryBlueGreen)
        }else{
            login_button.setTitle("LOGIN (\(timer))", for: .normal)
            login_button.isEnabled = false
            login_button.set(backgroundColor: .darkGray)
        }
    }
    
    func encryptUserData(key: String, iv: String, userData: String) -> String {
            let data = userData.data(using: .utf8)!
            let encrypted = try! AES(key: key, iv: iv).encrypt([UInt8](data))
            let encryptedData = Data(encrypted)
            return encryptedData.base64EncodedString()
    }
    
}
    
extension LoginViewController{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
    
extension LoginViewController: DataDidRecievedFromLogin{
    func didRecieveVeriftericationData(code: String) {
        self.mobileNumber_textField.isValid()
        self.password_textField.isValid()
    }
}

extension LoginViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .done{
            guard let nav = self.navigationController else { fatalError("navigation controller was not properly set") }
            DashboardWireframe(nav).navigate(to: .Dashboardscreen)
        }
        return true
    }
}

extension LoginViewController: GBAVerificationCodeDelegate{
    func ResendButton_tapped(sender: UIButton) {
        self.currentPresenter.resendVerificationCode {
            var countdown = 60
            
            sender.isEnabled = false
            sender.setTitleColor(GBAColor.darkGray.rawValue, for: .disabled)
            
            Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
                sender.setTitle("RESEND CODE (\(countdown))", for: .disabled)
                
                let _ = countdown-- < 0 ? (timer.invalidate(), (sender.isEnabled = true)): ((),())
            })
        }
    }
        
    func GBAVerification() {
        guard let nav = self.navigationController else{
            fatalError("Navigation View Controller was  not set in LiginViewController")
        }
        DashboardWireframe(nav).presentTabBarController()
    }
}
    

//For PrimaryUser
extension LoginViewController: SetUserLoginInfo {
    
    func addPrimaryLogInUser() {
        do {
            if let pinUser = GBARealm.object(ofType: UserKeyInfo.self, forPrimaryKey: "userID"){
                //Nothing needs be done.
                print(pinUser)
                print("PinUser already existing")
            } else {
                let newUser = UserKeyInfo()
                newUser.userNumber = "0"
                newUser.userPassword = "0"
                GBARealm.beginWrite()
                GBARealm.create(UserKeyInfo.self, value: newUser, update: true)
                try GBARealm.commitWrite()
            }
        } catch {
            print("error @addPrimaryLogInUser")
        }

    }

    func setNewPrimary(userName: String, userPassword: String) {
        let newUser = UserKeyInfo()
        
        newUser.userNumber = self.encryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userName)
        newUser.userPassword = self.encryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userPassword)
        
        do {
            try GBARealm.write {
                GBARealm.add(newUser, update: true)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
}
    
    
    
    
