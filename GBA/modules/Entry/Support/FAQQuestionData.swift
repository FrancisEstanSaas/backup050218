//
//  FAQQuestionData.swift
//  GBA
//
//  Created by Francis Jemuel Bergonia on 13/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

public class FAQquestion {
    
    public var customerQuestion: String
    public var customerAnswer: [FAQanswer]?
    
    init(question: String, answer: [FAQanswer]?) {
        self.customerQuestion = question
        self.customerAnswer = answer
    }
}


public class FAQanswer {
    public var customerAnswer : String
    
    init(answer: String) {
        self.customerAnswer = answer
    }
}
