//
//  SuportViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/20/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

private class textLabelCellView: UITableViewCell{
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}

class SupportViewController: EntryModuleViewController{
    
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet weak var FAQs_list: UITableView!
    
    private var scrollViewContainer = UIView()
    
    
    var FAQuestionsData: [FAQquestion?]?
    
    override func viewDidLoad() {
        self.addBackButton()
        self.view.backgroundColor = .white
        self.navigationController?.isNavigationBarHidden = false
        
        FAQs_list.delegate = self
        FAQs_list.dataSource = self
        FAQs_list.tableFooterView = UIView()
        
        FAQuestionsData = getData()
        FAQs_list.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Support"
    }
    
    @IBAction func contactSupport_tapped(_ sender: GBAButton) {
        self.title = " "
        self.presenter.wireframe.navigate(to: .ContactSupportscreen)
    }
    
    
    //calls data from FAQQuestionData.swift
    private func getData() -> [FAQquestion?] {
        //var data: [FAQquestion?] = []
        
        let firstAnswer = [FAQanswer(answer: "- GBA has a support function where the user can see contacts to our BPO Team or be able to directly submit a complaint.")]
        let firstQuestion = FAQquestion(question: "What do I do if my Mobile Number is already used?", answer: firstAnswer)
        
        let secondAnswer = [FAQanswer(answer: "- GBA has a support function where the user can see contacts to our BPO Team or be able to directly submit a complaint.")]
        let secondQuestion = FAQquestion(question: "What do I do if my Email is already used?", answer: secondAnswer)
        
        let thirdAnswer = [FAQanswer(answer: "- Check and validate if your number is correct. If it is correct, GBA has a support function where the user can see contacts to our BPO Team or be able to directly submit a complaint.")]
        let thirdQuestion = FAQquestion(question: "I'm not receiving a verification code.", answer: thirdAnswer)
        
        let fourthAnswer = [FAQanswer(answer: "- GBA has a Forgot Password function where the user can set a new password by first validating the mobile number with the received verification code. GBA has a support function where the user can see contacts to our BPO Team or be able to directly submit a complaint.")]
        let fourthQuestion = FAQquestion(question: "I forgot my Password. How can I retrieve or reset it?", answer: fourthAnswer)
        
        let fifthAnswer = [FAQanswer(answer: "- GBA has a Invite function where the user can send an invite to a mobile number that's not yet enrolled in GBA. We will send an SMS informing the user to install GBA as per the user's recommendation.")]
        let fifthQuestion = FAQquestion(question: "How do I invite my friends to use GBA using the application?", answer: fifthAnswer)
        
        return [firstQuestion, secondQuestion, thirdQuestion, fourthQuestion, fifthQuestion]
    }
    
    /*  Get parent cell for selected ExpansionCell  */
    private func getParentCellIndex(expansionIndex: Int) -> Int {
        
        var selectedCell: FAQquestion?
        var selectedCellIndex = expansionIndex
        
        while(selectedCell == nil && selectedCellIndex >= 0) {
            selectedCellIndex -= 1
            selectedCell = FAQuestionsData?[selectedCellIndex]
        }
        
        return selectedCellIndex
    }
    
    //For the expanding the cell
    private func expandCell(tableView: UITableView, index: Int) {
        // Expand Cell (add ExpansionCells
        if let answers = FAQuestionsData?[index]?.customerAnswer {
            for i in 1...answers.count {
                FAQuestionsData?.insert(nil, at: index + i)
                FAQs_list.insertRows(at: [NSIndexPath(row: index + i, section: 0) as IndexPath] , with: .top)
            }
        }
    }
    
    private func contractCell(tableView: UITableView, index: Int) {
        if let answers = FAQuestionsData?[index]?.customerAnswer {
            for _ in 1...answers.count {
                FAQuestionsData?.remove(at: index+1)
                FAQs_list.deleteRows(at: [NSIndexPath(row: index+1, section: 0) as IndexPath], with: .top)
                
            }
        }
    }
    
    override func backBtn_tapped() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
}


extension SupportViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let data = FAQuestionsData {
            return data.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Row is DefaultCell
        if let rowData = FAQuestionsData?[indexPath.row] {
            let defaultCell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell", for: indexPath) as! FAQDefaultCell
            //defaultCell.textLabel?.text = rowData.customerQuestion
            defaultCell.QuestionLabel?.text = rowData.customerQuestion
            return defaultCell
        }
            // Row is ExpansionCell
        else {
            if let rowData = FAQuestionsData?[getParentCellIndex(expansionIndex: indexPath.row)] {
                //  Create an ExpansionCell
                let expansionCell = tableView.dequeueReusableCell(withIdentifier: "ExpansionCell", for: indexPath) as! FAQExpansionCell
                
                //  Get the index of the parent Cell (containing the data)
                let parentCellIndex = getParentCellIndex(expansionIndex: indexPath.row)
                
                //  Get the index of the answer data (e.g. if there are multiple ExpansionCells
                let answerIndex = indexPath.row - parentCellIndex - 1
                
                expansionCell.AnswerLabel?.text = rowData.customerAnswer?[answerIndex].customerAnswer
                
                //  Set the cell's data
                //expansionCell.textLabel?.text = rowData.customerAnswer?[answerIndex].customerAnswer
                expansionCell.selectionStyle = .none
                return expansionCell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (FAQuestionsData?[indexPath.row]) != nil {
            
            // If user clicked last cell, do not try to access cell+1 (out of range)
            if(indexPath.row + 1 >= (FAQuestionsData?.count)!) {
                expandCell(tableView: tableView, index: indexPath.row)
            }
            else {
                // If next cell is not nil, then cell is not expanded
                if(FAQuestionsData?[indexPath.row+1] != nil) {
                    expandCell(tableView: tableView, index: indexPath.row)
                    // Close Cell (remove ExpansionCells)
                } else {
                    contractCell(tableView: tableView, index: indexPath.row)
                }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (FAQuestionsData?[indexPath.row]) != nil {
            return 40
        } else {
            return 200
        }
    }
    
}
