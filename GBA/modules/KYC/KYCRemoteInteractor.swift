//
//  KYCRemoteInteractor.swift
//  GBA
//
//  Created by EDI on 28/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

fileprivate enum APICalls: Router{
    
    var baseComponent: String { get { return "/public" } }

    case getKYCInfo(birthdate: String, citizenship: String, issued_image: String, current_street: String, current_city: String, current_state: String, current_postal_code: String, current_country: String, permanent_street: String, permanent_city: String, permanent_state: String, permanent_postal_code: String, permanent_country: String, use_permanent: String, billing_statement_image: String, source_of_funds: String, action: String)
    
    var route: Route {
        switch self {
        case .getKYCInfo( let birthdate, let citizenship, let issued_image, let current_street, let current_city, let current_state, let current_postal_code, let current_country, let permanent_street, let permanent_city, let permanent_state, let permanent_postal_code, let permanent_country, let use_permanent, let billing_statement_image, let source_of_funds, let action):
            return Route(method: .post,
                         suffix: "",
                         parameters:
                [ "birthdate"               :   birthdate,
                  "citizenship"             :   citizenship,
                  "issued_image"            :   issued_image,
                  "current_street"          :   current_street,
                  "current_city"            :   current_city,
                  "current_state"           :   current_state,
                  "current_postal_code"     :   current_postal_code,
                  "current_country"         :   current_country,
                  "permanent_street"        :   permanent_street,
                  "permanent_city"          :   permanent_city,
                  "permanent_state"         :   permanent_state,
                  "permanent_postal_code"   :   permanent_postal_code,
                  "permanent_country"       :   permanent_country,
                  "use_permanent"           :   use_permanent,
                  "billing_statement_image" :   billing_statement_image,
                  "source_of_funds"         :   source_of_funds,
                  "action"                  :   action
                ],
                         waitUntilFinished: true,
                         nonToken: true)
        }
    }
    
}

class KYCRemoteInteractor: RootRemoteInteractor{

    func submitKYCInfo(form: KYCInfoEntity, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        
        NetworkingManager.request(APICalls.getKYCInfo(birthdate: form.birthDate, citizenship: form.citezenship, issued_image: form.govtIDImage, current_street: form.currentStreet, current_city: form.currentCity, current_state: form.currentState, current_postal_code: form.currentZipCode, current_country: form.currentCountry, permanent_street: form.permanentStreet, permanent_city: form.permanentCity, permanent_state: form.permanentState, permanent_postal_code: form.permanentZipCode, permanent_country: form.permanentCountry, use_permanent: form.birthDate, billing_statement_image: form.billingStatement, source_of_funds: form.billingStatement, action: form.birthDate),
                                  successHandler:  { (reply, statusCode) in
                                    successHandler(reply, statusCode)
        })
        
        
    }
    
    
    
}
