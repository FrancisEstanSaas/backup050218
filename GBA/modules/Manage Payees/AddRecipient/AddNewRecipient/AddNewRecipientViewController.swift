//
//  AddNewRecipientViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/4/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class AddNewRecipientViewController: ManagePayeesModuleViewController{
    
    
    @IBOutlet weak var firstName_textField: GBATitledTextField!
    @IBOutlet weak var lastName_textField: GBATitledTextField!
    @IBOutlet weak var nickName_textField: GBATitledTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstName_textField
            .set(self)
            .set(max: 1)
            .set(alignment: .left)
            .set(placeholder: "First name")
            .set(textFieldIsEditable: false)
        
        lastName_textField
            .set(self)
            .set(max: 1)
            .set(alignment: .left)
            .set(placeholder: "Last name")
            .set(textFieldIsEditable: false)
        
        nickName_textField
            .set(self)
            .set(max: 1)
            .set(alignment: .left)
            .set(placeholder: "Nickname")
            .set(textFieldIsEditable: false)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = "Add Payee"
        
        firstName_textField
            .set(text: "Jayson")
        lastName_textField
            .set(text: "Craig")
        nickName_textField
            .set(text: "Jason")
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    @IBAction func addRecipientBtn_tapped(_ sender: GBAButton) {
        let name = NSAttributedString(string: "Jayson Craig",
        attributes: [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: GBAText.Size.subContent.rawValue)!])
        let message = NSAttributedString(string: " was added successfully to your ", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: GBAText.Size.subContent.rawValue)!])
        let payee = NSAttributedString(string: "Payee",
                                       attributes: [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: GBAText.Size.subContent.rawValue)!])
        
        let content = NSMutableAttributedString()
        content.append(name)
        content.append(message)
        content.append(payee)
        
        self.presenter.wireframe.presentSuccessPage(title: "Add Recipent", message: content)
    }
}

extension AddNewRecipientViewController: GBAFocusableInputViewDelegate{
    func GBAFocusableInput(view: UIView) {
        self.view.subviews.forEach{
            if let scrollView = $0 as? UIScrollView{
                $0.subviews.first?.subviews.forEach{
                    $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                    if self.focusdObjectFrame != nil {
                        let additionalMeasurements = scrollView.contentSize.height - UIScreen.main.bounds.height
                        additionalMeasurements < 0 ? () : (self.focusdObjectFrame?.size.height += additionalMeasurements)
                        return
                    }
                }
            }else{
                $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                if self.focusdObjectFrame != nil { return }
            }
        }
    }
}
