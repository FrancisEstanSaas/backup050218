//
//  NotificationsListViewController.swift
//  GBA
//
//  Created by Gladys Prado on 13/3/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation
import UIKit

class NotificationsListViewController: NotificationsViewController{
    
    @IBOutlet weak var tblNotificationList: UITableView!
    
    //API Connection
    fileprivate var notificationsHolder:[[String: Any]] = [[String: Any]]()
    
    private var currentDate = Date.Now
    
    private var currentPresenter: NotificationsListPresenter{
        guard let prsntr = self.presenter as? NotificationsListPresenter else{
            fatalError("Error parsing presenter in NotificationsListViewController")
        }
        return prsntr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblNotificationList.delegate = self
        tblNotificationList.dataSource = self
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Notifications"
        
        self.navigationController?.isNavigationBarHidden = false
        
        self.reloadTableView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    @objc override func backBtn_tapped() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
}

extension NotificationsListViewController: UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    /*func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var dateString = ""
        let sectionView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: self.view.frame.width, height: 40)))
        sectionView.backgroundColor = GBAColor.lightGray.rawValue
        
        guard let day = currentDate.day,
            let month = currentDate.monthAbv,
            let year = currentDate.year
            else{ fatalError("Problem in parsing date in dashboard viewController") }
        
        if section == 0{
            dateString = "TODAY • \(day) \(month.uppercased())"
        }else{
            dateString = "YESTERDAY \(day - 1) \(month.uppercased()) \(year)"
        }
        
        let date = UILabel()
            .set(color: GBAColor.darkGray.rawValue)
            .set(alignment: .left)
            .set(fontStyle: GBAText.Font.main(12).rawValue)
            .add(to: sectionView)
            .set(value: dateString)
        
        date.topAnchor.constraint(equalTo: sectionView.topAnchor, constant: 10).Enable()
        date.widthAnchor.constraint(equalTo: sectionView.widthAnchor, multiplier: 9/10).Enable()
        date.centerXAnchor.constraint(equalTo: sectionView.centerXAnchor).Enable()
        date.bottomAnchor.constraint(equalTo: sectionView.bottomAnchor).Enable()
        
        return sectionView
        
    }*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//       self.presenter.wireframe.navigate(to: .TransactionDetailscreen)
    }
    
    func reloadTableView(){
        self.currentPresenter.fetchRemoteTransactionHistoryList(in: 0, successHandler: {
            self.notificationsHolder = self.currentPresenter.notificationsHolder
            self.tblNotificationList.reloadData()
        })
    }
}

extension NotificationsListViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.notificationsHolder.count > 0 ){
            return self.notificationsHolder.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = Bundle.main.loadNibNamed("TransactionHistoryCellView", owner: self, options: nil)?.first as? TransactionHistoryCellView else{
            fatalError("Parsing TransactionHistoryCellView not properly parsed for TransactionHistoryViewController")
        }
        
        if (self.notificationsHolder.count > 0 ){
            let transactionDetails = self.notificationsHolder[indexPath.row] as NSDictionary
            print("NOTIFICATION MODULE ", transactionDetails)
            let transactionType = transactionDetails["type"] as? Int
            var header: String?
            if (transactionType == 1){
                header = "Reload"
            } else if (transactionType == 2) || (transactionType == 3){
                header = "Transfer"
            } else {
                header = "General"
            }
            
//            let transactionDate = transactionDetails["date"] as? String
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "hh:mm PM"
            
            cell.transactionTitle.text = header
            cell.transactionDate.text = transactionDetails["created_at"] as? String
            cell.transactionType.text = transactionDetails["content"] as? String
            cell.transactionAmount.text = ""
        }
        
        return cell
    }
}
