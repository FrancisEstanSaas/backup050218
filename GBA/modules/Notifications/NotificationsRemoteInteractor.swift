//
//  NotificationsRemoteInteractor.swift
//  GBA
//
//  Created by Gladys Prado on 28/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

fileprivate enum APICalls: Router{
    
    var baseComponent: String { get { return "public" } }
    
    case getAllNotifications
    
    var route: Route{
        switch self {
        case .getAllNotifications:
            return Route(method: .get,
                         suffix: "/notifications",
                         parameters: nil,
                         waitUntilFinished: true,
                         nonToken: false)
        }
    }
}

class NotificationsRemoteInteractor: RootRemoteInteractor{
    
    func getAllNotifications(successHandler: @escaping (JSON, ServerReplyCode)->Void){
        NetworkingManager.request(APICalls.getAllNotifications, successHandler: { (reply, replyCode) in
            successHandler(reply, replyCode)
        })
    }
}
