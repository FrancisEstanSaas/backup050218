//
//  NotificationsRootPresenter.swift
//  GBA
//
//  Created by Gladys Prado on 28/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

class NotificationsRootPresenter: RootPresenter{
    
    var wireframe: NotificationsWireframe
    var view: NotificationsViewController
    var interactor: (local: RootLocalInteractor, remote: NotificationsRemoteInteractor) = (RootLocalInteractor(), NotificationsRemoteInteractor())
    
    func set(view: NotificationsViewController){ self.view = view }
    
    init(wireframe: NotificationsWireframe, view: NotificationsViewController){
        self.wireframe = wireframe
        self.view = view
    }
}
