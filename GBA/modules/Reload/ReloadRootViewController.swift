//
//  ReloadRootViewController.swift
//  GBA
//
//  Created by Gladys Prado on 9/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class ReloadRootViewController: RootViewController {
    var presenter: ReloadRootPresenter{
        get{
            let prsntr = self._presenter as! ReloadRootPresenter
            return prsntr
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cancelButton = UIBarButtonItem(title: " ",
                                           style: .plain,
                                           target: self,
                                           action: #selector(tempCancelButton))
        self.navigationItem.leftBarButtonItem = cancelButton
        self.navigationItem.leftItemsSupplementBackButton = true
        
    }
    
    @objc func tempCancelButton() {
        guard let nav =  self.navigationController else {
            fatalError("")
        }
        
        AccountsWireframe(nav).navigate(to: .ListGBAWallets)
        self.tabBarController?.tabBar.isHidden = false
    }
}
