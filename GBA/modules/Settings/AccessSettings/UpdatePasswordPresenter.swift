//
//  UpdatePasswordPresenter.swift
//  GBA
//
//  Created by EDI on 30/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit

struct UpdatePasswordFormEntity: Decodable{
    var oldPassword: String = ""
    var newPassword: String = ""
    var newPasswordConfirm: String = ""
}

protocol DataDidReceiveFromUpdatePassword{
    func didReceiveNewPassword(code: String)
}

class UpdatePasswordPresenter: SettingsRootPresenter {
    
    var submittedForm: UpdatePasswordFormEntity? = nil
    var dataBridge: DataDidReceiveFromUpdatePassword? = nil

    
    func processNewPassword(submittedForm: UpdatePasswordFormEntity){

        guard let bridge = dataBridge else { fatalError("dataBridge was not implemented in UpdatePasswordPresenter") }

        self.interactor.remote.submitNewPassword(form: submittedForm, successHandler: {
            (reply, statusCode) in
            print(reply)
            print(statusCode)
            
            switch statusCode{
            case .fetchSuccess:
                guard let message = reply["message"] as? String else{
                    fatalError("message not found in server reply: [\(reply)]")
                }
                let messages = NSAttributedString(string: "Thank you for using GBA Mobile Banking Application. Your password has now been updated", attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica Neue", size: GBAText.Size.subContent.rawValue)!])
                
                let content = NSMutableAttributedString()
                content.append(messages)
                
                self.wireframe.presentSuccessPage(title: "Support", message: content, doneAction: {
                    self.wireframe.navigate(to: .AccessSettingsMainView)
                    //popToRootViewController(true)
                })
                
                bridge.didReceiveNewPassword(code: "\(message)")
                
                /**************************************************************************/
                
            case .badRequest:
                guard let messages = reply["message"] as? [String:Any] else{
                    fatalError("Message not found")
                }
                
                var message: String?
                
                messages.forEach{
                    message = ($0.value as? [String])?.first
                    return
                }
                self.showAlert(with: "Message", message: message ?? "message not found", completion: { () })
            default: break
            }
        })
    }
}
                /**************************************************************************/
