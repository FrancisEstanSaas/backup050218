//
//  EditProfilePresenter.swift
//  GBA
//
//  Created by EDI on 29/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit
import Alamofire

protocol DataDidReceiveFromEditProfile{
    func didReceiveResponse(code: String)
}

class EditProfilePresenter: SettingsRootPresenter {

    var submittedForm: EditProfile? = nil
    var dataBridge: DataDidReceiveFromEditProfile? = nil
    
    func processEditProfileInfo(submittedForm: EditProfile){
        self.submittedForm = submittedForm
        guard let bridge = dataBridge else { fatalError("dataBridge was not implemented in ContactSupportPresenter") }
        print(submittedForm)
        
        self.interactor.remote.submitEditProfileInfo(form: submittedForm, successHandler: {
            (reply, statusCode) in
            print(reply)
            print(statusCode)
            
            switch statusCode{
            case .fetchSuccess:
                guard let message = reply["message"] as? String else{
                    fatalError("message not found in server reply: [\(reply)]")
                }
                bridge.didReceiveResponse(code: "\(message)")
                
                
            case .notModified:
                guard let message = reply["message"] as? String else{
                    fatalError("message not found in server reply: [\(reply)]")
                }
                bridge.didReceiveResponse(code: "\(message)")
                
                
            case .badRequest:
                guard let messages = reply["message"] as? [String:Any] else{
                    fatalError("Message not found")
                }
                
                var message: String?
                
                messages.forEach{
                    message = ($0.value as? [String])?.first
                    return
                }
                self.showAlert(with: "Message", message: message ?? "message not found", completion: { () })
            default: break
            }
        })
        
    }
    
    
    
    func uploadProfilePicture(image: Data){
        guard let bridge = dataBridge else { fatalError("dataBridge was not implemented in ContactSupportPresenter") }

        //uploadProfilePicture(form: image, successHandler:
        self.interactor.remote.uploadProfileImage(form: image, successHandler: {
            (reply, statusCode) in
            print(reply)
            print(statusCode)

            switch statusCode{
            case .dataCreated:
                guard let message = reply["message"] as? String else{
                    fatalError("message not found in server reply: [\(reply)]")
                }
                bridge.didReceiveResponse(code: "\(message)")


            case .notModified:
                guard let message = reply["message"] as? String else{
                    fatalError("message not found in server reply: [\(reply)]")
                }
                bridge.didReceiveResponse(code: "\(message)")

            case .notFound:
                guard let message = reply["message"] as? String else{
                    fatalError("message not found in server reply: [\(reply)]")
                }
                bridge.didReceiveResponse(code: "\(message)")
                
                
            case .badRequest:
                guard let messages = reply["message"] as? [String:Any] else{
                    fatalError("Message not found")
                }

                var message: String?

                messages.forEach{
                    message = ($0.value as? [String])?.first
                    return
                }
                self.showAlert(with: "Message", message: message ?? "message not found", completion: { () })
            default: break
            }
        })
    }
}





