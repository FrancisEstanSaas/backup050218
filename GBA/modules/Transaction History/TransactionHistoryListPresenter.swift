//
//  TransactionHistoryListPresenter.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/29/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

protocol DataReceivedFromTransactionHistory{
    func didReceiveResponse(code: String)
}

class TransactionHistoryListPresenter: TransactionHistoryRootPresenter {
    var transactionHistoryHolder:[[String: Any]] = [[String: Any]]()
    
    func fetchRemoteTransactionHistoryList(in page: Int, successHandler: @escaping (()->Void)){
        self.interactor.remote.getAllTransactionHistories(successHandler: { (json, statusCode) in
            switch statusCode{
            case .noContent: break
            case .fetchSuccess:
                self.transactionHistoryHolder.removeAll()
                guard let transactions = json["data"] as? [[String: Any]] else {
                    return
                }
                
                for transaction in transactions {
                    let holder = transaction as [String: Any]
                    self.transactionHistoryHolder.append(holder)
                }
                successHandler()
                
            default: break
            }
        })
    }
}
