//
//  TransactionHistoryRootPresenter.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/29/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class TransactionHistoryRootPresenter: RootPresenter{
    
    var wireframe: TransactionHistoryWireframe
    var view: TransactionHistoryModuleViewController
    var interactor: (local: RootLocalInteractor, remote: TransactionHistoryRemoteInteractor) = (RootLocalInteractor(),TransactionHistoryRemoteInteractor())
    
    func set(view: TransactionHistoryModuleViewController){ self.view = view }
    
    init(wireframe: TransactionHistoryWireframe, view: TransactionHistoryModuleViewController){
        self.wireframe = wireframe
        self.view = view
    }
}

