//
//  PaySomeoneTransferScheduleViewController.swift
//  GBA
//
//  Created by Gladys Prado on 30/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class PaySomeoneTransferScheduleViewController: TransfersRootViewController {
    
    @IBOutlet weak var dateTransfer: UIDatePicker!
    @IBOutlet weak var lblDateSelected: UILabel!
    
    private var currentPresenter: PaySomeonePresenter{
        guard let prsntr = self.presenter as? PaySomeonePresenter else{
            fatalError("Error parsing presenter in ManagePayeeListController")
        }
        return prsntr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(tempNextButton))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        
        //set function for selecting dates
        dateTransfer.addTarget(self, action: #selector(datePickerChanged), for: UIControlEvents.valueChanged)
        dateTransfer.setValue(GBAColor.primaryBlueGreen.rawValue, forKey: "textColor")
        
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        
        let strDate = dateFormatter.string(from: currentDate)
        self.lblDateSelected.text = strDate
        self.dateTransfer.minimumDate = Date()
        
        self.addBackButton()
        
    }
    
    override func backBtn_tapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func addBackButton() {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: String.fontAwesomeIcon(name: .angleLeft), style: .plain, target: self, action: #selector(backBtn_tapped))
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .normal)
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .highlighted)
        
        UINavigationBar.appearance().barTintColor = GBAColor.primaryBlueGreen.rawValue
        UINavigationBar.appearance().tintColor = GBAColor.white.rawValue
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    @objc func tempNextButton() {
        currentPresenter.transferDate = self.lblDateSelected.text
        
        self.presenter.wireframe.navigate(to: .PaySomeoneTransactionSummaryView, with: self.currentPresenter)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.parent?.title = "Pay Someone"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @objc func datePickerChanged(dateTransfer: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        
        let strDate = dateFormatter.string(from: dateTransfer.date)
        self.lblDateSelected.text = strDate
    }
}
