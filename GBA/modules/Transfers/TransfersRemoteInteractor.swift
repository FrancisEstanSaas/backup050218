//
//  TransfersRemoteInteractor.swift
//  GBA
//
//  Created by Gladys Prado on 24/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

fileprivate enum TransferAPICalls: Router{
    
    var baseComponent: String { get {  return "/public" } }
    
    case transferToSomeone(recipient_id: Int, amount: Double, date: String)
    case fetchPayee
    case fetchPendingTransactions
    case fetchCompletedTransactions
    case fetchScheduledTransactions
    case fetchTransactionDetails(transaction_id: Int)
    case cancelPendingTransaction(transaction_id: Int)
    
    var route: Route {
        switch self {
        case .transferToSomeone(let recipient_id, let amount, let date):
            return Route(method: .post,
                         suffix: "/transactions/transfer",
                         parameters:
                [ "recipient_id"            : recipient_id,
                  "amount"                  : amount,
                  "date"                    : date],
                         waitUntilFinished: true)
        case .fetchPayee:
            return Route(   method              : .get,
                            suffix              : "/payees",
                            parameters          : nil,
                            waitUntilFinished   : true,
                            nonToken            : false)
        case .fetchPendingTransactions:
            return Route(   method              : .get,
                            suffix              : "/transactions/transfers/list/pending",
                            parameters          : nil,
                            waitUntilFinished   : true,
                            nonToken            : false)
        case .fetchCompletedTransactions:
            return Route(   method              : .get,
                            suffix              : "/transactions/transfers/list/completed",
                            parameters          : nil,
                            waitUntilFinished   : true,
                            nonToken            : false)
        case .fetchScheduledTransactions:
            return Route(   method              : .get,
                            suffix              : "/transactions/transfers/list/scheduled",
                            parameters          : nil,
                            waitUntilFinished   : true,
                            nonToken            : false)
        case .fetchTransactionDetails(let transaction_id):
            return Route(   method              : .get,
                            suffix              : "/transactions/transfers/\(transaction_id)",
                            parameters          : nil,
                            waitUntilFinished   : true,
                            nonToken            : false)
        case .cancelPendingTransaction(let transaction_id):
            return Route(   method              : .patch,
                            suffix              : "/transactions/transfers/cancellation/\(transaction_id)",
                            parameters          : nil,
                            waitUntilFinished   : true,
                            nonToken            : false)
        }
        
    }
}

class TransfersRemoteInteractor: RootRemoteInteractor{
    private var fieldTransferFormEntity: TransferFormEntity? = nil
    
    //Pay Someone
    func FetchPayeeList(successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(TransferAPICalls.fetchPayee, successHandler: {
            (reply, statusCode) in
            print(reply,statusCode)
            successHandler(reply, statusCode)
        })
    }
    
    func TransferPayToSomeone(form: TransferFormEntity, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        fieldTransferFormEntity = form
        
        NetworkingManager.request(TransferAPICalls.transferToSomeone(recipient_id: form.recipient_id,
                                                                     amount: form.amount,
                                                                     date: form.date!), successHandler: {
                                                                        (reply, statusCode) in
                                                                        successHandler(reply, statusCode)
                                                                        
        })
    }
    
    //Pending Transactions
    func FetchPendingTransactionsList(successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(TransferAPICalls.fetchPendingTransactions, successHandler: {
            (reply, statusCode) in
            print(reply,statusCode)
            successHandler(reply, statusCode)
        })
    }
    
    //Scheduled Transactions
    func FetchScheduledTransactionsList(successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(TransferAPICalls.fetchScheduledTransactions, successHandler: {
            (reply, statusCode) in
            print(reply,statusCode)
            successHandler(reply, statusCode)
        })
    }
    
    //Completed Transactions
    func FetchCompletedTransactionsList(successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(TransferAPICalls.fetchCompletedTransactions, successHandler: {
            (reply, statusCode) in
            print(reply,statusCode)
            successHandler(reply, statusCode)
        })
    }
    
    //Transaction Details
    func FetchTransactionDetails(transaction_id: Int, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(TransferAPICalls.fetchTransactionDetails(transaction_id: transaction_id), successHandler: {
            (reply, statusCode) in
            print(reply,statusCode)
            successHandler(reply, statusCode)
        })
    }
    
    //Cancel Pending Transaction
    func CancelPendingTransaction(transaction_id: Int, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(TransferAPICalls.cancelPendingTransaction(transaction_id: transaction_id), successHandler: {
            (reply, statusCode) in
            print(reply,statusCode)
            successHandler(reply, statusCode)
        })
    }
    
}
