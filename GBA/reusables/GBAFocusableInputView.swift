//
//  GBAFocusableInputView.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/17/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

protocol GBAFocusableInputViewDelegate{
    func GBAFocusableInput(view: UIView)
}


