//
//  GBANavigationViewController.swift
//  GBA
//
//  Created by Republisys on 05/02/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit

class GBANavigationController: UINavigationController{

    override func loadView() {
        UINavigationBar.appearance().barTintColor = GBAColor.primaryBlueGreen.rawValue
        UINavigationBar.appearance().tintColor = GBAColor.white.rawValue
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    
}


