//
//  GBADropdown.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/17/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class GBAPopOverButton: GBARootFocusableObject{
    
    private var _label: UILabel = UILabel()
    private var _imageIcon: UIImageView? = UIImageView()
    
    private var viewController: UIViewController? = nil
    fileprivate var _nextTextField: GBARootFocusableObject? = nil
    
    fileprivate(set) var placeholder: String = ""
    
    fileprivate(set) var text: String = ""{
        didSet{
            if text == "" {
                self._label
                    .set(value: self.placeholder)
                    .set(color: GBAColor.gray.rawValue)
            }else{
                self._label
                    .set(value: self.text)
                    .set(color: GBAColor.black.rawValue)
            }
            
            self._nextTextField?.becomeFirstResponder()
        }
    }
    
    fileprivate(set) var icon: UIImage?{
        didSet{
            self._imageIcon?.image = self.icon
            self.layoutIfNeeded()
        }
    }
    
    fileprivate(set) var underlineColor: GBAColor = GBAColor.lightGray{
        didSet{ self.setupXib() }
    }
    
    fileprivate(set) var parent: RootViewController? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupXib()
        
        self.isUserInteractionEnabled = true
        let tapped = UITapGestureRecognizer(target: self, action: #selector(openPopupTableview))
        self.addGestureRecognizer(tapped)
    }
    
    private func setupXib(){
        self.subviews.forEach{ $0.removeFromSuperview() }
        
        let size = self.frame.size
        let underline = UIView()
        
        self.backgroundColor = .clear
        self.clipsToBounds = true
        
        _label = UILabel()
            .set(color: text == "" ? GBAColor.gray.rawValue : GBAColor.black.rawValue)
            .set(lines: 1)
            .set(fontStyle: GBAText.Font.main(GBAText.Size.subContent.rawValue).rawValue)
            .set(value: text == "" ? placeholder : text)
        
        _label.contentMode = .bottomLeft
        _label.translatesAutoresizingMaskIntoConstraints = false
        
        if icon != nil {
            icon?.accessibilityFrame.size = CGSize(width: size.height * 0.2, height: size.height * 0.2)
            _imageIcon = UIImageView(image: icon)
            _imageIcon?.contentMode = .scaleAspectFit
            _imageIcon?.translatesAutoresizingMaskIntoConstraints = false
        }
        else { _imageIcon = nil }
        
        underline.backgroundColor = self.underlineColor.rawValue
        underline.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(underline)
        self.addSubview(_label)
        icon == nil ? () : (self.addSubview(_imageIcon!))
        
        if let image = _imageIcon{
            image.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.6).Enable()
            image.widthAnchor.constraint(equalTo: image.heightAnchor, constant: 1.3).Enable()
            image.leadingAnchor.constraint(equalTo: self.leadingAnchor).Enable()
            image.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -14).Enable()
            image.trailingAnchor.constraint(equalTo: _label.leadingAnchor, constant: -10).Enable()
        }
        
        _imageIcon == nil ? _label.leadingAnchor.constraint(equalTo: self.leadingAnchor).Enable().final() : ()
        _label.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12).Enable()
        _label.trailingAnchor.constraint(equalTo: self.trailingAnchor).Enable()
        _label.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.6).Enable()
        
        underline.leadingAnchor.constraint(equalTo: self.leadingAnchor).Enable()
        underline.trailingAnchor.constraint(equalTo: self.trailingAnchor).Enable()
        underline.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12).Enable()
        underline.heightAnchor.constraint(equalToConstant: 1).Enable()
        
    }
    
    @objc private func openPopupTableview(){
//        guard let parent = self.parent as? RegistrationViewController else { fatalError("Popup parent was never sent") }
        guard let parent = self.parent else { fatalError("Popup parent was never sent") }
        guard let vc = viewController else { fatalError("ViewController was not set for PopOverButton") }
        
        vc.modalPresentationStyle = .popover
        
        vc.popoverPresentationController?.permittedArrowDirections = .up
        vc.popoverPresentationController?.delegate = parent
        vc.popoverPresentationController?.sourceView = self
        vc.popoverPresentationController?.sourceRect = self.bounds
        
        parent.view.resignFirstResponder()
        parent.present(vc, animated: true, completion: nil)
        
    }
    
    override func becomeFirstResponder() -> Bool {
        super.becomeFirstResponder()
        self.openPopupTableview()
        return true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setupXib()
    }
}

extension GBAPopOverButton{
    @discardableResult
    func set(placeholder: String)->Self{
        self.placeholder = placeholder
        return self
    }
    
    @discardableResult
    func set(text: String)->Self{
        self.text = text
        return self
    }
    
    @discardableResult
    func set(icon: UIImage)->Self{
        self.icon = icon
        return self
    }
    
    @discardableResult
    func set(parent: RootViewController)->Self{
        self.parent = parent
        return self
    }
    
    @discardableResult
    func set(viewController: UIViewController)->Self{
        self.viewController = viewController
        return self
    }
    
    @discardableResult
    func set(next: GBARootFocusableObject)->Self{
        self._nextTextField = next
        return self
    }
    
    @discardableResult
    func set(backgroundColor: GBAColor)->Self{
        self.backgroundColor = backgroundColor.rawValue
        return self
    }
    
    @discardableResult
    func set(underlineColor: GBAColor)->Self{
        self.underlineColor = underlineColor
        self.setupXib()
        return self
    }
    
    @discardableResult
    func set(frontColor: GBAColor)->Self{
        self._label.set(color: frontColor.rawValue)
        self.setupXib()
        return self
    }
    
}


