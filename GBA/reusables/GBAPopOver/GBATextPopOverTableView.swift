//
//  GBATitlePopOverTableView.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/22/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift

class GBATextCellView: UITableViewCell{
    
    var text_Label: UILabel = UILabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        
        self.addSubview(text_Label)
        
        text_Label.translatesAutoresizingMaskIntoConstraints = false
        text_Label
            .set(size: self.frame.width * 0.04)
            .set(lines: 0)
        
        self.text_Label.topAnchor.constraint(equalTo: self.topAnchor).Enable()
        self.text_Label.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).Enable()
        self.text_Label.bottomAnchor.constraint(equalTo: self.bottomAnchor).Enable()
        self.text_Label.trailingAnchor.constraint(equalTo: self.trailingAnchor).Enable()
    }
}

class GBATextPopOverTableView: GBAPopOverRootViewController{
    
    fileprivate(set) var textOptions:[String] = [String]()
    fileprivate var textColor: UIColor = GBAColor.gray.rawValue
    
    override var preferredContentSize: CGSize{
        get{
            let multiplier = self.textOptions.count > 4 ? 4: self.textOptions.count
            return CGSize(width: self.view.width, height: 44 * multiplier.Float)
        }
        set { super.preferredContentSize = newValue }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        
        let cell = GBATextCellView()
        
        cell.text_Label.text = textOptions[index]
        cell.text_Label.textColor = textColor
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textOptions.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.dismiss(animated: true) {
            guard let del = self.delegate else { fatalError("delegate was not properly set for GBAPopOverContryTableView") }
            
            guard let cellView = tableView.cellForRow(at: indexPath) else { fatalError("cellView can't be found") }
            
            del.GBAPopOver(didSelect: cellView)
        }
    }
}

extension GBATextPopOverTableView{
    @discardableResult
    func set(options: [String])->Self{
        self.textOptions = options
        return self
    }
    
    @discardableResult
    func set(textColor: UIColor)->Self{
        self.textColor = textColor
        return self
    }
}

