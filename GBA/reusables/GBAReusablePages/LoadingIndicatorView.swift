//
//  LoadingIndicatorView.swift
//  GBA
//
//  Created by Gladys Prado on 9/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation
import UIKit

class LoadingIndicatorView: UIView {
    fileprivate var navigationController: UINavigationController?
    
    fileprivate let backView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
    fileprivate let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    
    convenience init() {
        self.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        initializeView()
    }
    
    fileprivate func initializeView() {
        backView.backgroundColor = .black
        backView.alpha = 0.6
        backView.layer.cornerRadius = 10
        
        self.addSubview(backView)
        self.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backView.center = self.center
        activityIndicatorView.center = self.center
    }
}
