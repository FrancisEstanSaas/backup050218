//
//  Constant.swift
//  Prototype-Global_Banking_Access
//
//  Created by Emmanuel Albania on 10/20/17.
//  Copyright © 2017 Emmanuel Albania. All rights reserved.
//

import UIKit


enum GBAColor{
    case green
    case red
    case orange
    
    case primaryBlueGreen
    case secondaryBlueGreen
    
    case lightGray
    case gray
    case midGray
    case darkGray
    
    case black
    case white
    
    case custom(hexa: UInt, alpha: CGFloat)
    
    var rawValue: UIColor{
        switch self {
        case .green:                    return UIColor(hexa: 0x19d764)
        case .red:                      return UIColor(hexa: 0xfa124e)
        case .orange:                   return UIColor(hexa: 0xff9802)
            
        case .primaryBlueGreen:         return UIColor(hexa: 0x25a1b2)
        case .secondaryBlueGreen:       return UIColor(hexa: 0x136d80)
            
        case .lightGray:                return UIColor(hexa: 0xefeff4)
        case .gray:                     return UIColor(hexa: 0x8e8e93)
        case .midGray:                  return UIColor(hexa: 0xc7c7cc)
        case .darkGray:                 return UIColor.darkGray
            
        case .black:                    return UIColor(hexa: 0x111111, alpha: 1)
        case .white:                    return UIColor.white
            
        case let .custom(hexa, alpha):  return UIColor(hexa: hexa, alpha: alpha)
        }
    }
}

