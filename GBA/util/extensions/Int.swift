



import UIKit

extension Int{
    var Float: CGFloat{ get{ return CGFloat(self) } }
    var StringValue: String { get { return String(describing: self) } }
}
