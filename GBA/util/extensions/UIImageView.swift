



import UIKit

extension UIImageView{
    @discardableResult
    func roundCorners()->Self{
        var radius: CGFloat? = nil
        
        self.frame.width > self.frame.height ?
            (radius = self.frame.width / 2):
            (radius = self.frame.height / 2)

        self.layer.cornerRadius = radius!
        return self
    }
}

