



import UIKit

enum GBABackgroundColor{
    case clear
}

enum GBAGradientLayer{
    case shadedBlueGreen
}

extension UIViewController{
    
    func apply(backgroundColor: GBABackgroundColor){
        switch backgroundColor {
        case .clear:
            self.view.backgroundColor = .clear
        }
    }
    
    func apply(gradientLayer: GBAGradientLayer){
        switch gradientLayer {
        case .shadedBlueGreen:
            self.apply(backgroundColor: .clear)
            
            let layer = CAGradientLayer()
                .set(frame: UIScreen.main.bounds)
                .set(colors: [.primaryBlueGreen, .secondaryBlueGreen])
                .set(start: CGPoint(x: 1, y: 0))
                .set(end: CGPoint(x: 1, y: 1))
                .set(locations: [0.0, 0.5])
            
            self.view.layer.insertSublayer(layer, at: 0)
        }
    }
}
